#include "menu.h"
#include "Item.h"
#include "DefaultItems.h"
#include "FileHandler.h"
#include <fstream>
#include <iostream>
using std::string; using std::cout; using std::cin; using std::endl;

//method used to sell items
void sellItems(string fileName){						
	std::ifstream myFile(fileName);
}


//method used to restock items
void restockItems(){					
	
}


//method used to add new items
void addNewItem(){		
	
	int itemChoice; 	//will be used in switch case for item constructor
	
	cout << "1. CD \n2. DVD \n3. Book \n4. Magazine" << endl; 
	cout << "Enter the number of the item type you want to add:";
	
	while(!(cin >> itemChoice)){		//input validation block
		
		cout << "Error: please enter one of the choices" << endl;
		
		cin.clear();
		
		cin.ignore(100, '\n');		
		
	}
	
	while(itemChoice < 1 || itemChoice > 4){
		cout << "Error: please enter one of the choices" << endl;
		
		cin.clear();
		
		cin.ignore(100, '\n');
			
		cin >> itemChoice;
		
		while(!(cin >> itemChoice)){
		
			cout << "Error: please enter one of the choices" << endl;
			
			cin.clear();
			
			cin.ignore(100, '\n');		
		
		}
	}
	
	switch (itemChoice) {
		
		case 1:
			string title; 
			int yearReleased; 
			double price; 
			int stockAvailable; 
			string artist; 
			string label;
			int tracks; 
			int id;
			
			cout << "Choose a unique integer ID for your CDP:" << endl;
			
			while(!(cin >> id)){		//input validation block
		
				cout << "Error: please enter an integer" << endl;
				
				cin.clear();
				
				cin.ignore(100, '\n');		
				
			} 
			while(idExists("recordFile.csv", (id + ""))){
				cout << "Error: please enter one of the choices" << endl;
				
				cin.clear();
				
				cin.ignore(100, '\n');
					
				cin >> itemChoice;
				
				while(!(cin >> itemChoice)){
					
					cout << "Error: please enter one of the choices" << endl;
						
					cin.clear();
						
					cin.ignore(100, '\n');		
					
				}
			}
		
	}
	
}


//method used to update stock of an item
void updateStock(){
	
}


//method used to display sales report		
void viewSalesReport(){
	
}

void viewInventory(){
	
}