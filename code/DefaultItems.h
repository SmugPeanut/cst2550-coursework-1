#ifndef _DEFAULTITEMS_H_
#define _DEFAULTITEMS_H_

#include "Item.h"

using std::string;

//CD class
class CD: protected Item {
	
	protected:
		string artist;
		string recordLabel;
		int trackCount;
		

	public:
		CD(string title, int yearReleased, double price, int stockAvailable, string artist, string label, int tracks, int id);
		string getArtist();

		string getLabel();
		
		int getTrackCount();
};
//DVD class
class DVD: protected Item {
	
	protected:
		int runtime;
	
	public:
		DVD(string title, int yearReleased, double price, int stockAvailable, int runtime, int id);
		int getRuntime();
};
//Book class
class Book: protected Item {
	
	protected:
		string author;
		string publisher;
		int pageCount;
		
	public:
		Book(string title, int yearReleased, double price, int stockAvailable, string author, string publisher, int pageCount, int id);
		string getAuthor();
		
		string getPublisher();
		
		int getPageCount();
};

//Magazine class
class Magazine: protected Item {
	
	protected:
		string publisher;
		int issue;
		
	public:
		Magazine(string title, int yearReleased, double price, int stockAvailable, string publisher, int issue, int id);
		
		string getPublisher();
		
		int getIssue();
		
};

#endif