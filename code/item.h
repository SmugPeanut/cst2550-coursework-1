#ifndef _ITEM_H_
#define _ITEM_H_

#include <iostream>

using std::string;

//item header file
//I'm using this as a base class for all the main items sold in the shop
//I.e. CDs, DVDs, books and magazines all inherit the properties of the Item class
class Item {

 protected:
	string title;
	int yearReleased;
	double price;
	int stockAvailable;
	int sales;
	int id;
	string itemType;
  
 public: 
	Item(string title, int yearReleased, double price, int stockAvailable, int id);
	
	string getTitle();
	
	int getYearReleased();
	
	double getPrice();
	
	void setPrice(double newPrice);
	
	int getStock();
	
	void setStock(int newStock);
	
	int getSales();
	
	void saleCount(int sales);
	
	void setId(int newId);
	
	int getId();
	
	string getItemType();
};

#endif