#include "DefaultItems.h"

using std::string;

/* Defining CD constructor and methods*/
CD::CD(string title, int yearReleased, double price, int stockAvailable, string artist, string label, int tracks, int id): Item(title, yearReleased, price, stockAvailable, id) {
	
	this->title = title;
	this->yearReleased = yearReleased;
	this->price = price;
	this->stockAvailable = stockAvailable;
	this->artist = artist;
	this->recordLabel = label;
	this->trackCount = tracks;
	this->itemType = "CD";
	
}

string CD::getArtist(){
	return this->artist;
}

string CD::getLabel(){
	return this->recordLabel;
}
		
int CD::getTrackCount(){
	return this->trackCount;
} /* CD definitions complete */



/* Defining DVD constructor and methods */
DVD::DVD(string title, int yearReleased, double price, int stockAvailable, int runtime, int id): Item(title, yearReleased, price, stockAvailable, id) {
	
	this->title = title;
	this->yearReleased = yearReleased;
	this->price = price;
	this->stockAvailable = stockAvailable;
	this->runtime = runtime;
	this->itemType = "DVD";
}

int DVD::getRuntime() {
	return this->runtime;
}
/* DVD definitions complete */



/* Defining book constructor and methods */
Book::Book(string title, int yearReleased, double price, int stockAvailable, string author, string publisher, int pageCount, int id): Item(title, yearReleased, price, stockAvailable, id) {
	
	this->title = title;
	this->yearReleased = yearReleased;
	this->price = price;
	this->stockAvailable = stockAvailable;
	this->author = author;
	this->publisher = publisher;
	this->pageCount = pageCount;
	this->itemType = "Book";
}

string Book::getAuthor(){
	return this->author;
}
		
string Book::getPublisher(){
	return this->publisher;
}
		
int Book::getPageCount(){
	return this->pageCount;
}
/* Book definitions complete */



/* Defining Magazine constructor and methods */
Magazine::Magazine(string title, int yearReleased, double price, int stockAvailable, string publisher, int issue, int id): Item(title, yearReleased, price, stockAvailable, id) {
	
	this->title = title;
	this->yearReleased = yearReleased;
	this->price = price;
	this->stockAvailable = stockAvailable;
	this->publisher = publisher;
	this->issue = issue;
	this->itemType = "Magazine";
}

string Magazine::getPublisher(){
	return this->publisher;
}

int Magazine::getIssue(){
	return this->issue;
}
/* Magazine definitions complete */