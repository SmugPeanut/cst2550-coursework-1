#ifndef _MENU_H_
#define _MENU_H_

//This class is used to handle the main menu options, 
//which provide the program's main functionality

void sellItems();

void restockItems();
	
void addNewItem();
	
void updateStock();
	
void viewSalesReport();

void viewInventory();



#endif