#include "Item.h"

using std::string;

//Definitions of the Item class

Item::Item(string title, int yearReleased, double price, int stockAvailable, int id){ 		//Item constructor
	
	this->title = title;
	this->yearReleased = yearReleased;
	this->price = price;
	this->stockAvailable = stockAvailable;
	this->id = id;
}

string Item::getTitle(){
	return this->title;
}
	
int Item::getYearReleased(){
	return this->yearReleased;
}

double Item::getPrice(){
	return this->price;
}
	
void Item::setPrice(double newPrice){
	this->price = newPrice;
}
	
int Item::getStock(){
	return this->stockAvailable;
}
	
void Item::setStock(int newStock){
	this->stockAvailable = newStock;
}

int Item::getSales(){
	return this->sales;
}
	
void Item::saleCount(int sales){
	this->sales += sales;
}

int Item::getId(){
	return this->id;
}


string Item::getItemType(){
	return this->itemType;
}