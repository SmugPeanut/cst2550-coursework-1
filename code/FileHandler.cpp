#include <iostream>
#include <fstream>
#include <string>
#include <vector>

std::string readFile(std::string fileName){		//used to read individual records in the .csv file
	
	std::vector<std::vector<std::string>> CDs;
	std::vector<std::vector<std::string>> DVDs;
	std::vector<std::vector<std::string>> Books;
	std::vector<std::vector<std::string>> Magazines;
	
	std::ifstream myFile(fileName);
	myFile.open("recordFile.csv", std::fstream::in);
	return 0;
}

void modifyRecord(){							//used to modify a record's value in an entry

}

void newEntry(std::string fileName){			//used to create a new item entry in the stock file
	
}

std::string readEntry(){
	return 0;
}

bool doesFileExist(std::string fileName){
	std::fstream myFile(fileName);
    return myFile.good();
}
